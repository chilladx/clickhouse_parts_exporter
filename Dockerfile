## Build image
FROM python:3.11 as poetry

COPY pyproject.toml poetry.lock clickhouse_parts_exporter.py README.md /

RUN pip install --no-cache-dir 'poetry==1.6.1'

ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1

RUN poetry install

## Run image
FROM python:3.11-slim

COPY --from=poetry /.venv /.venv
COPY --from=poetry /clickhouse_parts_exporter.py /
ENV PATH="/.venv/bin:$PATH"

ENTRYPOINT ["python", "/clickhouse_parts_exporter.py"]
