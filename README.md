# Clickhouse parts exporter

Here is an exporter to expose metrics about your Clickhouse parts in a prometheus format.

## Installation

This project uses `poetry` as package management / virtualenv integration. Installation should be straight forward:

```sh
$ poetry shell
$ poetry install
```

## Usage

Usage is recommanded using the Docker image. You can add the image in your `docker-compose.yml` file, along with some configuration variables:

| Variable            | Description                                | Default |
| ------------------- | ------------------------------------------ | ------- |
| CLICKHOUSE_HOST     | Hostname of the Clickhouse server          |         |
| CLICKHOUSE_PORT     | HTTP port of the Clickhouse server         | 8123    |
| CLICKHOUSE_LOGIN    | Login to connecft the Clickhouse server    |         |
| CLICKHOUSE_PASSWORD | Password to connecft the Clickhouse server |         |

Your `docker-compose.yml` file could look like:

```yaml
ch_parts_exporter:
  image: registry.gitlab.com/chilladx/clickhouse_parts_exporter/clickhouse_parts_exporter:latest
  environment:
    CLICKHOUSE_HOST: clickhouse
    CLICKHOUSE_LOGIN: foo
    CLICKHOUSE_PASSWORD: bar
  ports:
    - "8000:8000/tcp"
```

You can find a complete example with the provided [docker-compose.yml](docker-compose.yml) file.

## Metrics

| Metric          | Description              | Labels          |
| --------------- | ------------------------ | --------------- |
| chpe_table_size | Size in bytes of a table | database, table |
| chpe_table_rows | Count of rows of a table | database, table |

## License

[MIT](LICENSE)
