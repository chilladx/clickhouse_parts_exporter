#!/usr/bin/env python
# -*- coding: utf8 -*-

import os
import logging
import time
import click
import clickhouse_connect
from prometheus_client import start_http_server
from prometheus_client.core import GaugeMetricFamily, REGISTRY

logger = logging.getLogger("root")


def configure_logger(loglevel):
    """Configures the logger, setting formatter and handler"""
    level = {"info": logging.INFO, "debug": logging.DEBUG}.get(
        loglevel.lower(), logging.INFO
    )

    logger.setLevel(level)
    handler = logging.StreamHandler()
    handler.setLevel(level)

    formatter = logging.Formatter(
        "%(asctime)s %(levelname)s [%(process)d] - %(message)s"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    logger.info(f"Log level: {loglevel}")


class ClickhousePartExporterCollector(object):
    def __init__(self) -> None:
        pass

    def collect(self):
        clickhouse_host = os.environ.get("CLICKHOUSE_HOST")
        table_sizes = GaugeMetricFamily(
            "chpe_table_size",
            "Size in bytes of Clickhouse tables",
            labels=["database", "table"],
        )
        table_rows = GaugeMetricFamily(
            "chpe_table_rows",
            "Count of rows in Clickhouse tables",
            labels=["database", "table"],
        )

        logger.debug(f"Connecting Clickhouse server {clickhouse_host}")
        try:
            client = clickhouse_connect.get_client(host=clickhouse_host)
        except clickhouse_connect.driver.exceptions.OperationalError:
            logger.error(f"unable to reach {clickhouse_host}")
        else:
            result = client.query(
                """
                SELECT database, table, sum(bytes) AS size, sum(rows) as rows
                FROM system.parts
                WHERE active
                GROUP BY database, table
                ORDER BY sum(bytes) DESC
                """
            )

            for stat in result.result_rows:
                database = stat[0] if stat[0] is not None else ""
                table = stat[1] if stat[1] is not None else ""
                table_sizes.add_metric(labels=[database, table], value=stat[2])
                table_rows.add_metric(labels=[database, table], value=stat[3])

        yield table_sizes
        yield table_rows


@click.command()
@click.option(
    "--loglevel",
    help="Log level",
    type=click.Choice(["INFO", "DEBUG"], case_sensitive=False),
    default="INFO",
)
@click.option("--port", help="Exporter port", type=int, default=8000)
def start_exporter(loglevel, port):
    configure_logger(loglevel=loglevel)

    start_http_server(port)
    REGISTRY.register(ClickhousePartExporterCollector())

    while True:
        time.sleep(1)


if __name__ == "__main__":
    start_exporter()
